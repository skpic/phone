import { HelloPage } from '../pages/hello/hello';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';


export function getPages() {
    return {
        'hello': HelloPage,
        'tabs': TabsPage,
        'login': LoginPage
    };
}

export function getPageFor(name) {
    return getPages()[name];
}