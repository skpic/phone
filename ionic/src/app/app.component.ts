import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { LoginPage } from '../pages/login/login';
import * as pages from '../directives/pages';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild('content') public content: NavController;

  rootPage = LoginPage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  goPage(name) {

    debugger

    this.content.setRoot(pages.getPageFor(name), {}, { animate: false });
  }
}
