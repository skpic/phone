import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
	selector: 'login',
	templateUrl: 'login.html'
})

export class LoginPage implements OnInit {

	ngOnInit() { }

	constructor(public actionSheetCtrl: ActionSheetController, public alert: AlertController) {
	}
	presentActionSheet() {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Modify your album',
			buttons: [
				{
					text: 'Destructive',
					role: 'destructive',
					handler: () => {
						console.log('Destructive clicked');
					}
				}, {
					text: 'Archive',
					handler: () => {
						console.log('Archive clicked');
					}
				}, {
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		});
		actionSheet.present();
	}
}