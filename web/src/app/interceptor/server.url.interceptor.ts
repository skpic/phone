import { Interceptor, InterceptedRequest, InterceptedResponse } from 'ng2-interceptors';
import { Component, ViewEncapsulation, ViewChild, Injectable } from '@angular/core';

import { ToastComponent, ToastService } from "ngx-weui/toast";

@Injectable()
export class ServerURLInterceptor implements Interceptor {

    constructor(private srv: ToastService) { }

    loading: ToastComponent;

    public interceptBefore(request: InterceptedRequest): InterceptedRequest {
        // 修改请求
        /* request.url = "http://localhost:8080/ovit-java-framework/" + request.url; */

        if (!this.loading || !this.loading._showd) {

             this.loading = this.srv['loading'](null, 1000000).onShow();
        }

        console.log('我是开头')
        return request;
    }
    public interceptAfter(response: InterceptedResponse): InterceptedResponse {

        console.log(this.loading)

        if (this.loading && this.loading._showd) {

            let loading = this.loading;

            setTimeout(function () {
                loading.onHide();
            }, 200);
        }

        console.log('我是结尾')

        return response;
    }

}