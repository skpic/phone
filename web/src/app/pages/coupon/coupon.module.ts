import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WeUiModule } from 'ngx-weui';
import { AppModule } from '../../app.module';

import { SharedModule } from '../../shared/shared.module';

import { SessionStorage } from "../../services/session.storage.service";
import { CouponService } from "../../services/coupon.service";

import { CouponListComponent } from './coupon-list/coupon-list.component';

import { CouponRoutes } from './coupon.routes';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    RouterModule.forChild(CouponRoutes),
    WeUiModule.forRoot()
  ],
  declarations: [
    CouponListComponent
  ],
  providers: [
    SessionStorage,
    CouponService
  ]
})
export class CouponModule {

}
