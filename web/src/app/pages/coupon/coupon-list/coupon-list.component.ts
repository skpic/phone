import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ToptipsComponent, ToptipsService } from "ngx-weui/toptips";

import { SessionStorage } from "../../../services/session.storage.service";
import { CouponService } from "../../../services/coupon.service";

import { ApiResponseModel } from "../../../model/api-response.model";
import { WechatCouponModel } from "../../../model/wechat.coupon.model";

import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-coupon-list',
  templateUrl: './coupon-list.component.html',
  styleUrls: ['./coupon-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CouponListComponent implements OnInit {

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private ss: SessionStorage,
    private srv: ToptipsService,
    private couponService: CouponService
  ) { }

  @ViewChild('toptips') toptips: ToptipsComponent;

  couponList: Array<WechatCouponModel> = new Array<WechatCouponModel>();

  ngOnInit() {

    let openid = this.ss.get(environment.openid);

    let coupon = new WechatCouponModel();
    coupon.openId = openid;

    this.couponService.list(coupon).subscribe(
      data => {
        data.list.forEach(d => {
          d.showDesc = false;
          this.couponList.push(d);
        });

        console.log(this.couponList)
      },
      error => console.error(error)
    );
  }
}
