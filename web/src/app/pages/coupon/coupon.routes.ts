import { RouterModule } from '@angular/router';

import { CouponListComponent } from './coupon-list/coupon-list.component';

export const CouponRoutes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: 'list',
        component: CouponListComponent,
        data: { title: '优惠券' }
    },
]
