import { RouterModule } from '@angular/router';

import { VaccinationListComponent } from './vaccination-list/vaccination-list.component';
import { VaccinationDetailComponent } from './vaccination-detail/vaccination-detail.component';

export const VaccinationRoutes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: 'list',
        component: VaccinationListComponent,
        data: { title: '接种记录' }
    },
    {
        path: 'detail/:id',
        component: VaccinationDetailComponent,
        data: { title: '疫苗详情' }
    }
]
