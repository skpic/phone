import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, Subscriber } from 'rxjs/Rx';

import { DoseService } from "../../../services/dose.service";
import { VaccineService } from "../../../services/vaccine.service";
import { SessionStorage } from '../../../services/session.storage.service';
import { ToastComponent, ToastService } from "ngx-weui/toast";
import { ToptipsComponent, ToptipsService } from "ngx-weui/toptips";
import { PickerData, PickerOptions, PickerService } from 'ngx-weui/picker';
import { DialogService, DialogConfig, DialogComponent } from "ngx-weui/dialog";

import { ApiResponseModel } from "../../../model/api-response.model";
import { DoseModel } from "../../../model/dose.model";
import { VaccineModel } from "../../../model/vaccine.model";
import { WechatVaccineModel } from "../../../model/wechat.vaccine.model";

import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-vaccination-detail',
  templateUrl: './vaccination-detail.component.html',
  styleUrls: ['./vaccination-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VaccinationDetailComponent implements OnInit {

  constructor(
    private el: ElementRef,
    private picker: PickerService,
    private dialog: DialogService,
    private toastService: ToastService,
    private route: ActivatedRoute,
    private router: Router,
    private ss: SessionStorage,
    private vaccineService: VaccineService,
    private doseService: DoseService
  ) { }

  id: string = '';
  model: VaccineModel = new VaccineModel();
  list: Array<DoseModel> = new Array<DoseModel>();
  @ViewChild('toptips') toptips: ToptipsComponent;

  ngOnInit() {
    this.init();
  }

  init() {

    let openId = this.ss.get(environment.openid);

    this.route.paramMap.switchMap((params: ParamMap) => {

      this.id = params.get('id');

      return this.vaccineService.get(this.id, openId)
    }).subscribe((model: ApiResponseModel<VaccineModel>) => {

      if (!model.response || model.response.status != 200) {

        this.toptips.type = 'warn';
        this.toptips.text = '获取数据失败！';
        this.toptips.onShow();
        return;
      }

      this.model = model.detail;
      this.list = this.model.dostList;

      this.list.forEach(function (value, index) {
        if (value.type == '1') {
          value.buttonCount = 2;
        } else {
          value.buttonCount = 1;
        }
      })

      /*console.log(JSON.stringify(this.model))
      console.log(JSON.stringify(this.list)) */
    }, error => console.error(error));
  }

  inoculation(doseId: string) {
    this.picker.showDateTime('date').subscribe((res: any) => {

      let model = new WechatVaccineModel();

      model.doseId = doseId;
      model.openId = this.ss.get(environment.openid);
      model.vaccineId = this.id;
      model.vaccineTime = res.value;

      this.vaccineService.inoculation(model).subscribe(
        data => {
          if (data.response && data.response.status == 200) {
            this.init();
          }
        },
        error => console.error(error)
      );
    });
  }

  notInoculation(id: string) {
    let config = Object.assign({}, <DialogConfig>{
      skin: 'ios',
      backdrop: true,
      content: '是否设为未接种',
      cancel: '否',
      confirm: '是'
    });
    this.dialog.show(config).subscribe((res: any) => {

      if (res.value) {
        this.vaccineService.notInoculation(id).subscribe(
          data => {
            if (data.response && data.response.status == 200) {
              this.init();
            }
          },
          error => console.error(error)
        );
      }
    });
  }

  changeInoculationDate(id: string, date: Date) {

    this.picker.showDateTime('date', null, new Date(date)).subscribe((res: any) => {

      let model = new WechatVaccineModel();

      model.wechatVaccineId = id;
      model.vaccineTime = res.value;

      this.vaccineService.changeInoculationDate(model).subscribe(
        data => {
          if (data.response && data.response.status == 200) {
            this.init();
          } else {
            console.log(data.response.message)
          }
        },
        error => console.error(error)
      );
    });
  }

  ngOnDestroy() {
    this.picker.destroyAll();
    this.dialog.destroyAll();
  }
}
