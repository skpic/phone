import { Component, OnInit, ViewEncapsulation, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { InfiniteLoaderComponent } from 'ngx-weui/infiniteloader';

import { DoseModel } from '../../../model/dose.model';
import { SessionStorage } from '../../../services/session.storage.service';
import { DoseService } from '../../../services/dose.service';

import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-vaccination-list',
  templateUrl: './vaccination-list.component.html',
  styleUrls: ['./vaccination-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VaccinationListComponent implements OnInit {

  constructor(private el: ElementRef,
    private doseService: DoseService,
    private ss: SessionStorage) { }

  list: Array<DoseModel> = new Array<DoseModel>();

  ngOnInit() {


    this.search();
  }

  search() {

    let openId = this.ss.get(environment.openid);

    let dose = new DoseModel();
    dose.openId = openId;

    this.doseService.list(dose)
      .subscribe(
      data => {
        data.list.forEach(d => this.list.push(d));
      },
      error => console.error(error)
      );

    console.log(this.list)
  }
}
