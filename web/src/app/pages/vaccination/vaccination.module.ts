import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WeUiModule } from 'ngx-weui';

import { SharedModule } from '../../shared/shared.module';
import { VaccinationRoutes } from './vaccination.routes';

import { VaccineService } from '../../services/vaccine.service';
import { DoseService } from "../../services/dose.service";
import { WXService } from '../../services/wx.service';
import { SessionStorage } from '../../services/session.storage.service';

import { VaccinationListComponent } from './vaccination-list/vaccination-list.component';
import { VaccinationDetailComponent } from './vaccination-detail/vaccination-detail.component';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    RouterModule.forChild(VaccinationRoutes),
    WeUiModule.forRoot()
  ],
  declarations: [
    VaccinationListComponent,
    VaccinationDetailComponent
  ],
  providers: [
    WXService,
    SessionStorage,
    VaccineService,
    DoseService
  ]
})
export class VaccinationModule {

}
