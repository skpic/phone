import { RouterModule } from '@angular/router';

import { SignedSearchComponent } from './signed-search/signed-search.component';
import { SignedComponent } from './signed/signed.component';

export const SignedRoutes = [
    {
        path: '',
        redirectTo: 'input',
        pathMatch: 'full'
    },
    {
        path: 'input',
        component: SignedComponent,
        data: { title: '签约查询' }
    },
    {
        path: 'search/:idcard/:name',
        component: SignedSearchComponent,
        data: { title: '签约查询' }
    }
]
