import { Http } from '@angular/http';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ToptipsComponent, ToptipsService } from "ngx-weui/toptips";

import { SessionStorage } from "../../../services/session.storage.service";
import { CouponService } from "../../../services/coupon.service";

import { ApiResponseModel } from "../../../model/api-response.model";
import { WechatCouponModel } from "../../../model/wechat.coupon.model";

import { environment } from "../../../../environments/environment";
import { Logger } from "../../../error";

@Component({
  selector: 'app-signed',
  templateUrl: './signed.component.html',
  styleUrls: ['./signed.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SignedComponent implements OnInit {

  constructor(
    private location: Location,
    private http: Http,
    private route: ActivatedRoute,
    private router: Router,
    private ss: SessionStorage,
    private srv: ToptipsService
  ) { }

  idcard: string = "";
  name: string = "";

  ngOnInit() {
  }


  onSave() {

    this.router.navigate(['pages/signed/search/', this.idcard, this.name]);
  }
}
