import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignedSearchComponent  } from './signed-search.component';

describe('SignedSearchComponent', () => {
  let component: SignedSearchComponent;
  let fixture: ComponentFixture<SignedSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignedSearchComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignedSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
