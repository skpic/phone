import { Http } from '@angular/http';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ToptipsComponent, ToptipsService } from "ngx-weui/toptips";

import { SessionStorage } from "../../../services/session.storage.service";
import { CouponService } from "../../../services/coupon.service";

import { ApiResponseModel } from "../../../model/api-response.model";
import { WechatCouponModel } from "../../../model/wechat.coupon.model";
import { SignedModel } from "../../../model/signed.model";

import { environment } from "../../../../environments/environment";
import { Logger } from "../../../error";

@Component({
  selector: 'app-signed-search',
  templateUrl: './signed-search.component.html',
  styleUrls: ['./signed-search.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SignedSearchComponent implements OnInit {

  constructor(
    private location: Location,
    private http: Http,
    private route: ActivatedRoute,
    private router: Router,
    private ss: SessionStorage,
    private srv: ToptipsService
  ) { }

  @ViewChild('toptips') toptips: ToptipsComponent;
  content: string = '';
  showContent: boolean = true;
  signedList: Array<SignedModel> = [];

  ngOnInit() {

    this.route.paramMap
      .switchMap((params: ParamMap) => {
        let idcard = params.get('idcard');
        let name = params.get('name')

        var url: string = `http://ywjywx.top-doctors.net/iejdj/os/HisQuery/QianYueFWXX.aspx?ShenFenZH=${idcard}&XingMing=${name}&laiyuan=pc`;

        return this.http.get(url);

      })
      .catch(Logger.handleError)
      .subscribe((res) => {

        let body = res._body;

        let reg = new RegExp('<div class="main">([^]+?)</div>');

        let result = reg.exec(body);

        if (result != null && result.length > 0) {

          let urlReg = new RegExp('<sapn class="right">([^]+?)</span>', 'g');

          let uls = result[0].match(new RegExp('<ul>([^]+?)</ul>', 'g'))

          if (uls != null && uls.length > 0) {

            for (var key in uls) {
              if (uls.hasOwnProperty(key)) {
                var element = uls[key];

                let contents = element.match(new RegExp('<sapn class="right">([^]+?)</span>', 'g'))

                if (contents.length > 0) {

                  let signed = new SignedModel();

                  signed.name = contents[0].replace('<sapn class="right">', '').replace('</span>', '');
                  signed.status = contents[1].replace('<sapn class="right">', '').replace('</span>', '');
                  signed.startDate = contents[2].replace('<sapn class="right">', '').replace('</span>', '');
                  signed.endDate = contents[3].replace('<sapn class="right">', '').replace('</span>', '');
                  signed.count = contents[4].replace('<sapn class="right">', '').replace('</span>', '');
                  signed.doctor = contents[5].replace('<sapn class="right">', '').replace('</span>', '');
                  signed.department = contents[6].replace('<sapn class="right">', '').replace('</span>', '');

                  this.signedList.push(signed);
                } else {
                  this.showContent = false;
                }

              }
            }

          } else {
            this.showContent = false;
          }
        } else {
          this.showContent = false;
        }

        console.log(this.signedList)
      });
  }

}
