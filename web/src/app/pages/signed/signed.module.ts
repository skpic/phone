import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WeUiModule } from 'ngx-weui';
import { AppModule } from '../../app.module';

import { SharedModule } from '../../shared/shared.module';

import { SessionStorage } from "../../services/session.storage.service";

import { SignedSearchComponent } from './signed-search/signed-search.component';
import { SignedComponent } from './signed/signed.component';

import { SignedRoutes } from './signed.routes';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    RouterModule.forChild(SignedRoutes),
    WeUiModule.forRoot()
  ],
  declarations: [
    SignedComponent,
    SignedSearchComponent
  ],
  providers: [
    SessionStorage
  ]
})
export class SignedModule {

}
