import { RouterModule } from '@angular/router';

import { PagesComponent } from './pages.component';

export const PagesRoutes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            {
                path: 'team',
                loadChildren: './team/team.module#TeamModule'
            },
            {
                path: 'vaccination',
                loadChildren: './vaccination/vaccination.module#VaccinationModule'
            },
            {
                path: 'coupon',
                loadChildren: './coupon/coupon.module#CouponModule'
            },
            {
                path: 'consultation',
                loadChildren: './consultation/consultation.module#ConsultationModule'
            },
            {
                path: 'signed',
                loadChildren: './signed/signed.module#SignedModule'
            }
        ]
    }
];