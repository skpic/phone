import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { WeUiModule } from 'ngx-weui';
import { SharedModule } from '../shared/shared.module';

import { PagesRoutes } from './pages.routes';
import { PagesComponent } from './pages.component';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    RouterModule.forChild(PagesRoutes),
    WeUiModule.forRoot()
  ],
  declarations: [
    PagesComponent
  ],
  providers: [
  ],
  exports: [RouterModule]
})
export class PagesModule {

}
