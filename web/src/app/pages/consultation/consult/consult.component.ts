import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UploaderOptions, FileItem, Uploader, UploaderHeaders } from "ngx-weui";
import { Observable } from 'rxjs/Rx';

import { SessionStorage } from "../../../services/session.storage.service";
import { ConsultationService } from "../../../services/consultation.service";
import { DoctorService } from "../../../services/doctor.service";

import { ApiResponseModel } from "../../../model/api-response.model";
import { ConsultationModel } from "../../../model/consultation.model";
import { DoctorModel } from "../../../model/doctor.model";

import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-consult',
  templateUrl: './consult.component.html',
  styleUrls: ['./consult.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConsultComponent implements OnInit {

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private ss: SessionStorage,
    private consultationService: ConsultationService,
    private doctorService: DoctorService
  ) { }

  doctor: DoctorModel = new DoctorModel();
  model: ConsultationModel = new ConsultationModel();

  ngOnInit() {

    this.route.paramMap
      .switchMap((params: ParamMap) => this.consultationService.get(params.get('id')))
      .subscribe((model: ApiResponseModel<ConsultationModel>) => {
        this.model = model.detail

        this.doctorService.get(this.model.doctorId).subscribe(
          data => {

            this.doctor = data.detail;
          },
          error => console.error(error)
        );
      }, error => console.error(error));

  }

  toPay(): void {
    console.log(this.model);
    this.router.navigate(['pages/consultation/doctor/list/']);

    /* this.consultationService.add(this.model).subscribe(
      data => {
        if (this.model.price == 0) {
          
        } else {
           this.router.navigate("/pages/consultation/pay");
        }
      },
      error => console.error(error)
    ); */

  }

  onSave() {

    this.model.status = "1";
    this.model.consultationStatus = "1";
    this.model.consultationDate = new Date();
    this.model.pictUrls = [];

    this.uploader.queue.forEach((value, index) => {
      this.model.pictUrls.push(value.tag);
    })

    console.log(this.model);
    console.log(this.uploader);

    this.consultationService.edit(this.model).subscribe(data => {
      console.log(data);
      //保存成功
      this.router.navigate(["/pages/consultation/doctor/list"]);
    }, error => console.error(error))
  }

  /* 以下是图片上传相关代码 */
  uploader: Uploader = new Uploader(<UploaderOptions>{
    url: `${environment.domain}/upload/save`,
    method: 'POST',
    alias: 'file',
    auto: true,
    limit: 5,
    params: { type: "consult", consultationId: this.model.consultationId },
    onFileQueued: function () {
      //console.log('onFileQueued', arguments);
    },
    onFileDequeued: function () {
      //console.log('onFileDequeued', arguments);
    },
    onStart: function () {
      //console.log('onStart', arguments);
    },
    onCancel: function () {
      //console.log('onCancel', arguments);
    },
    onFinished: function () {
      //console.log('onFinished', arguments);
    },
    onUploadStart: function () {
      //console.log('onUploadStart', arguments);
    },
    onUploadProgress: function () {
      //console.log('onUploadProgress', arguments);
    },
    onUploadSuccess: function () {
      console.log('onUploadSuccess', arguments);

      //上传图片完毕 设置model中的图片路径地址为服务器返回路径
      let picUrl = JSON.parse(arguments[1]).detail;
      arguments[0].tag = picUrl;
    },
    onUploadComplete: function () {
      //console.log('onUploadComplete', arguments)
    },
    onUploadError: function () {
      //console.log('onUploadError', arguments);
    },
    onUploadCancel: function () {
      //console.log('onUploadCancel', arguments);
    },
    onError: function () {
      //console.log('onError', arguments);
    }
  });

  img: any;
  imgShow: boolean = false;
  onGallery(item: any) {
    this.img = [{ file: item._file, item: item }];
    this.imgShow = true;
  }

  onDel(item: any) {
    console.log(item);
    this.uploader.removeFromQueue(item.item);
  }

}
