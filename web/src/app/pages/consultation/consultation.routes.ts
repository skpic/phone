import { RouterModule } from '@angular/router';

import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { DoctorDetailComponent } from './doctor-detail/doctor-detail.component';
import { ConsultationBuyComponent } from './consultation-buy/consultation-buy.component';
import { ConsultComponent } from './consult/consult.component';
import { PayComponent } from './pay/pay.component';
import { PaySuccessComponent } from './pay-success/pay-success.component';
import { PayFailedComponent } from './pay-failed/pay-failed.component';
import { ConsultationListComponent } from './consultation-list/consultation-list.component';
import { ConsultationDetailComponent } from './consultation-detail/consultation-detail.component';

export const ConsultationRoutes = [
    {
        path: '',
        redirectTo: 'doctor/list',
        pathMatch: 'full'
    },
    {
        path: 'doctor/list',
        component: DoctorListComponent,
        data: { title: '家医在线' }
    },
    {
        path: 'doctor/detail/:id',
        component: DoctorDetailComponent,
        data: { title: '医生详情' }
    },
    {
        path: 'buy/:id',
        component: ConsultationBuyComponent,
        data: { title: '购买咨询服务' }
    },
    {
        path: 'consult/:id',
        component: ConsultComponent,
        data: { title: '填写咨询内容' }
    },
    {
        path: 'pay/:id',
        component: PayComponent,
        data: { title: '在线支付' }
    },
    {
        path: 'pay/success/:id',
        component: PaySuccessComponent,
        data: { title: '支付成功' }
    },
    {
        path: 'pay/failed/:id',
        component: PayFailedComponent,
        data: { title: '支付失败' }
    },
    {
        path: 'detail/:id',
        component: ConsultationDetailComponent,
        data: { title: '咨询详情' }
    },
    {
        path: 'list',
        component: ConsultationListComponent,
        data: { title: '我的咨询' }
    }
]
