import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ToptipsComponent, ToptipsService } from "ngx-weui/toptips";

import { SessionStorage } from "../../../services/session.storage.service";
import { DoctorService } from "../../../services/doctor.service";
import { CouponService } from "../../../services/coupon.service";
import { ConsultationService } from "../../../services/consultation.service";

import { ApiResponseModel } from "../../../model/api-response.model";
import { DoctorModel } from "../../../model/doctor.model";
import { WechatCouponModel } from "../../../model/wechat.coupon.model";
import { ConsultationModel } from "../../../model/consultation.model";

import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-consultation-buy',
  templateUrl: './consultation-buy.component.html',
  styleUrls: ['./consultation-buy.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConsultationBuyComponent implements OnInit {

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private ss: SessionStorage,
    private srv: ToptipsService,
    private doctorService: DoctorService,
    private couponService: CouponService,
    private consultationService: ConsultationService
  ) { }

  @ViewChild('toptips') toptips: ToptipsComponent;

  doctor: DoctorModel = new DoctorModel();
  model: ConsultationModel = new ConsultationModel();
  _status: boolean = false;
  couponList: Array<WechatCouponModel> = new Array<WechatCouponModel>();
  choiceCoupon: WechatCouponModel = new WechatCouponModel();

  ngOnInit() {

    this.route.paramMap
      .switchMap((params: ParamMap) => this.doctorService.get(params.get('id')))
      .subscribe((model: ApiResponseModel<DoctorModel>) => this.doctor = model.detail);

    let openid = this.ss.get(environment.openid);

    let coupon = new WechatCouponModel();
    coupon.openId = openid;

    this.couponService.list(coupon).subscribe(
      data => {
        data.list.forEach(d => this.couponList.push(d));

        /*  if (this.couponList.length > 0) this.choiceCoupon = this.couponList[0];
         else { */
        this.choiceCoupon.name = "使用优惠券";
        this.choiceCoupon.price = 0;
        //}
        this.calculatePrice();
        console.log(this.couponList)
      },
      error => console.error(error)
    );

    this.model.basicSex = '1';
    this.model.status = '0';
    this.model.openId = openid;
  }

  toggleOpened(): void {
    this._status = true;
  }

  toggleChoice(): void {
    this._status = false;
    this.calculatePrice();
    console.log(this.choiceCoupon)
  }

  calculatePrice(): void {
    this.model.price = this.doctor.price - this.choiceCoupon.price;
    if (!this.model.price || this.model.price <= 0) this.model.price = 0;
  }

  onSave(): void {

    this.model.couponId = this.choiceCoupon.wechatCouponId;
    this.model.doctorId = this.doctor.doctorId;
    this.model.doctorName = this.doctor.name;

    console.log(this.model);

    this.consultationService.add(this.model).subscribe(
      data => {

        console.log(data)

        if (!data.response || data.response.status != 200) {
          this.srv['warn']('操作失败！');
          return;
        }

debugger;

        if (this.model.price == 0) {
          this.router.navigate(['pages/consultation/consult/', data.id]);
        } else {
          this.router.navigate(['pages/consultation/pay/', data.id]);
        }
      },
      error => console.error(error)
    );


  }
}
