import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { DoctorService } from "../../../services/doctor.service";
import { ApiResponseModel } from "../../../model/api-response.model";
import { DoctorModel } from "../../../model/doctor.model";

@Component({
  selector: 'app-doctor-detail',
  templateUrl: './doctor-detail.component.html',
  styleUrls: ['./doctor-detail.component.css']
})
export class DoctorDetailComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private doctorService: DoctorService
  ) { }

  model: DoctorModel = new DoctorModel();

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.doctorService.get(params.get('id')))
      .subscribe((model: ApiResponseModel<DoctorModel>) => this.model = model.detail, error => console.error(error));

  }

}
