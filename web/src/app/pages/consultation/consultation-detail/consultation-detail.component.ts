import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ConsultationService } from "../../../services/consultation.service";
import { ApiResponseModel } from "../../../model/api-response.model";
import { ConsultationModel } from "../../../model/consultation.model";
import { DoctorModel } from "../../../model/doctor.model";

@Component({
  selector: 'app-consultation-detail',
  templateUrl: './consultation-detail.component.html',
  styleUrls: ['./consultation-detail.component.css']
})
export class ConsultationDetailComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private consultationService: ConsultationService
  ) { }

  model: ConsultationModel = new ConsultationModel();
  doctor: DoctorModel = new DoctorModel();

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.consultationService.get(params.get('id')))
      .subscribe((model: ApiResponseModel<ConsultationModel>) => {
        this.model = model.detail;
        this.doctor = model.detail.doctor;
        console.log(this.model);
      }, error => console.error(error));

  }

  img: any;
  imgShow: boolean = false;
  onGallery(item: any) {
    this.img = item;
    this.imgShow = true;
  }

}
