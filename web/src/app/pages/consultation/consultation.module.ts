import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WeUiModule } from 'ngx-weui';

import { SharedModule } from '../../shared/shared.module';

import { ConsultationRoutes } from './consultation.routes';

import { DoctorService } from '../../services/doctor.service';
import { ConsultationService } from "../../services/consultation.service";
import { CouponService } from "../../services/coupon.service";
import { WXService } from '../../services/wx.service';
import { SessionStorage } from '../../services/session.storage.service';

import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { DoctorDetailComponent } from './doctor-detail/doctor-detail.component';
import { ConsultationBuyComponent } from './consultation-buy/consultation-buy.component';
import { ConsultComponent } from './consult/consult.component';
import { PayComponent } from './pay/pay.component';
import { PaySuccessComponent } from './pay-success/pay-success.component';
import { PayFailedComponent } from './pay-failed/pay-failed.component';
import { ConsultationListComponent } from './consultation-list/consultation-list.component';
import { ConsultationDetailComponent } from './consultation-detail/consultation-detail.component';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    RouterModule.forChild(ConsultationRoutes),
    WeUiModule.forRoot()
  ],
  declarations: [
    DoctorListComponent,
    DoctorDetailComponent,
    ConsultationBuyComponent,
    ConsultComponent,
    PayComponent,
    PaySuccessComponent,
    PayFailedComponent,
    ConsultationListComponent,
    ConsultationDetailComponent
  ],
  providers: [
    WXService,
    SessionStorage,
    DoctorService,
    ConsultationService,
    CouponService
  ]
})
export class ConsultationModule {

}
