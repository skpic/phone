import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayFailedComponent } from './pay-failed.component';

describe('PayFailedComponent', () => {
  let component: PayFailedComponent;
  let fixture: ComponentFixture<PayFailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PayFailedComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
