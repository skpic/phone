import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { WXService } from "../../../services/wx.service";
import { SessionStorage } from "../../../services/session.storage.service";
import { ConsultationService } from "../../../services/consultation.service";

import { ApiResponseModel } from "../../../model/api-response.model";
import { ConsultationModel } from "../../../model/consultation.model";

import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PayComponent implements OnInit {

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private ss: SessionStorage,
    private wxService: WXService,
    private consultationService: ConsultationService
  ) { }

  model: ConsultationModel = new ConsultationModel();
  _status: boolean = false;
  paytype: string = '1';

  ngOnInit() {

    this.route.paramMap
      .switchMap((params: ParamMap) => this.consultationService.get(params.get('id')))
      .subscribe((model: ApiResponseModel<ConsultationModel>) => this.model = model.detail, error => console.error(error));
  }

  toPay(): void {

    this.consultationService.pay(this.model.consultationId)
      .subscribe((model: ApiResponseModel<string>) => {

        this.wxService.onBridgeReady(model)
          .subscribe((code: any) => {

            console.error(code);
            this.router.navigate(['pages/consultation/consult/', this.model.consultationId]);
          }, error => {
            console.error(error);
            this.router.navigate(['pages/consultation/pay/failed/', this.model.consultationId]);
          });

      }, error => console.error(error));
  }
}
