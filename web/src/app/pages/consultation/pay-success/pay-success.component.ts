import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { SessionStorage } from "../../../services/session.storage.service";
import { ConsultationService } from "../../../services/consultation.service";

import { ApiResponseModel } from "../../../model/api-response.model";
import { ConsultationModel } from "../../../model/consultation.model";

import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-pay-success',
  templateUrl: './pay-success.component.html',
  styleUrls: ['./pay-success.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PaySuccessComponent implements OnInit {

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private ss: SessionStorage,
    private consultationService: ConsultationService
  ) { }

  model: ConsultationModel = new ConsultationModel();
  _status: boolean = false;
  paytype: string = '1';

  ngOnInit() {

    this.route.paramMap
      .switchMap((params: ParamMap) => this.consultationService.get(params.get('id')))
      .subscribe((model: ApiResponseModel<ConsultationModel>) => this.model = model.detail, error => console.error(error));
  }

  toPay(): void {
    console.log(this.model);

    this.router.navigate(["/pages/consultation/consult/", this.model.consultationId]);

    /* this.consultationService.add(this.model).subscribe(
      data => {
        if (this.model.price == 0) {
          
        } else {
           this.router.navigate("/pages/consultation/pay");
        }
      },
      error => console.error(error)
    ); */


  }
}
