import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { InfiniteLoaderComponent } from 'ngx-weui/infiniteloader';

import { ConsultationModel } from '../../../model/consultation.model';
import { SessionStorage } from '../../../services/session.storage.service';
import { ConsultationService } from '../../../services/consultation.service';

import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-consultation-list',
  templateUrl: './consultation-list.component.html',
  styleUrls: ['./consultation-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConsultationListComponent implements OnInit {

  constructor(private el: ElementRef, private ss: SessionStorage, private consultationService: ConsultationService) { }

  list: Array<ConsultationModel> = new Array<ConsultationModel>();
  index: number = 1;
  size: number = 10;
  total: number = 0;
  loadType: string = 'loading';
  showLoading: boolean = false;
  openid: string = '';

  @ViewChild('notPayBlock')
  notPayBlock: TemplateRef<any> | null = null;
  @ViewChild('payNotConsultBlock')
  payNotConsultBlock: TemplateRef<any> | null = null;

  ngOnInit() {

    this.openid = this.ss.get(environment.openid);

    this.search(null);
  }

  search(comp: InfiniteLoaderComponent) {
    var model = new ConsultationModel();
    model.pageIndex = this.index;
    model.pageSize = this.size;
    model.openId = this.openid;

    if (this.index <= 1) this.list = new Array<ConsultationModel>();

    this.consultationService.list(model)
      .subscribe(
      data => {
        data.list.forEach(d => this.list.push(d));

        this.total = data.total;
        if (comp != null) {
          if (data.total <= this.size * (this.index)) {
            comp.setFinished();
          } else {
            comp.resolveLoading();
          }
        }
      },
      error => console.error(error)
      );
  }

  onLoadMore(comp: InfiniteLoaderComponent) {

    if (this.total <= this.size * (this.index)) {
      comp.setFinished();
    } else {
      this.index++;
      this.search(comp);
    }
  }

}
