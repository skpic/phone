import { Component, OnInit, ViewEncapsulation, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { InfiniteLoaderComponent } from 'ngx-weui/infiniteloader';

import { DoctorModel } from '../../../model/doctor.model';
import { DoctorService } from '../../../services/doctor.service';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DoctorListComponent implements OnInit {

  constructor(private el: ElementRef, private doctorService: DoctorService) { }

  list: Array<DoctorModel> = new Array<DoctorModel>();
  index: number = 1;
  size: number = 10;
  total: number = 0;

  ngOnInit() {
    this.search(null);
  }

  search(comp: InfiniteLoaderComponent) {
    var model = new DoctorModel();
    model.pageIndex = this.index;
    model.pageSize = this.size;

    if (this.index <= 1) this.list = new Array<DoctorModel>();

    this.doctorService.list(model)
      .subscribe(
      data => {
        data.list.forEach(d => this.list.push(d));
        this.total = data.total;
        if (comp != null) {
          if (data.total <= this.size * (this.index)) {
            comp.setFinished();
          } else {
            comp.resolveLoading();
          }
        }
      },
      error => console.error(error)
      );
  }

  onSelect(type) {
    this.index = 1;
    this.search(null);
  }

  onLoadMore(comp: InfiniteLoaderComponent) {

    if (this.total <= this.size * (this.index)) {
      comp.setFinished();
    } else {
      this.index++;
      this.search(comp);
    }
  }

}
