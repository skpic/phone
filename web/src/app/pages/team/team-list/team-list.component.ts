import { Component, OnInit, ViewEncapsulation, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { InfiniteLoaderComponent } from 'ngx-weui/infiniteloader';

import { TeamModel } from '../../../model/team.model';
import { TeamService } from '../../../services/team.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TeamListComponent implements OnInit {

  constructor(private el: ElementRef,
    private teamService: TeamService
  ) { }

  list: Array<TeamModel> = new Array<TeamModel>();
  type: string = '1';
  index: number = 1;
  size: number = 10;
  total: number = 0;
  loadType: string = 'loading';
  showLoading: boolean = false;

  ngOnInit() {

    this.search(null);
  }

  search(comp: InfiniteLoaderComponent) {
    var model = new TeamModel();
    model.type = this.type;
    model.pageIndex = this.index;
    model.pageSize = this.size;

    if (this.index <= 1) this.list = new Array<TeamModel>();

    this.teamService.list(model)
      .subscribe(
      data => {
        data.list.forEach(d => this.list.push(d));
        this.total = data.total;
        if (comp != null) {
          if (data.total <= this.size * (this.index)) {
            comp.setFinished();
          } else {
            comp.resolveLoading();
          }
        }
      },
      error => console.error(error)
      );
  }

  onSelect(type) {
    this.index = 1;
    this.type = type;
    this.search(null);
  }

  onLoadMore(comp: InfiniteLoaderComponent) {

    if (this.total <= this.size * (this.index)) {
      comp.setFinished();
    } else {
      this.index++;
      this.search(comp);
    }
  }

}
