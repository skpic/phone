import { RouterModule } from '@angular/router';

import { TeamListComponent } from './team-list/team-list.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { TeamSignedComponent } from './team-signed/team-signed.component';

export const TeamRoutes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: 'list',
        component: TeamListComponent,
        data: { title: '签约团队' }
    },
    {
        path: 'detail/:id',
        component: TeamDetailComponent,
        data: { title: '团队介绍' }
    },
    {
        path: 'signed',
        component: TeamSignedComponent,
        data: { title: '下载' }
    }
]
