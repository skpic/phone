import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { TeamService } from "../../../services/team.service";
import { TeamMemberService } from "../../../services/team-member.service";

import { ApiResponseModel } from "../../../model/api-response.model";
import { TeamModel } from "../../../model/team.model";
import { TeamMemberModel } from "../../../model/team-member.model";

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private teamService: TeamService,
    private teamMemberService: TeamMemberService
  ) { }

  model: TeamModel = new TeamModel();
  list: Array<TeamMemberModel> = new Array<TeamMemberModel>();

  ngOnInit() {

    this.route.paramMap
      .switchMap((params: ParamMap) => this.teamService.get(params.get('id')))
      .subscribe((model: ApiResponseModel<TeamModel>) => this.model = model.detail, error => console.error(error));


    this.route.paramMap
      .switchMap((params: ParamMap) => {
        var model = new TeamMemberModel();
        model.teamId = params.get('id');

        return this.teamMemberService.list(model)
      })
      .subscribe(data => {
        data.list.forEach(d => this.list.push(d));
      },
      error => console.error(error));

  }

}
