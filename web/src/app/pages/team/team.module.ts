import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WeUiModule } from 'ngx-weui';

import { SharedModule } from '../../shared/shared.module';

import { TeamRoutes } from './team.routes';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { TeamSignedComponent } from './team-signed/team-signed.component';

import { TeamService } from './../../services/team.service';
import { TeamMemberService } from "./../../services/team-member.service";

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    RouterModule.forChild(TeamRoutes),
    WeUiModule.forRoot()
  ],
  declarations: [
    TeamListComponent,
    TeamDetailComponent,
    TeamSignedComponent
  ],
  providers: [
    TeamService,
    TeamMemberService
  ],
  exports: [RouterModule]
})
export class TeamModule {

}
