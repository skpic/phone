import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamSignedComponent } from './team-signed.component';

describe('TeamSignedComponent', () => {
  let component: TeamSignedComponent;
  let fixture: ComponentFixture<TeamSignedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamSignedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamSignedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
