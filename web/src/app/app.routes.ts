import { RouterModule } from '@angular/router'

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';

export const appRoutes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent,
        data: { title: '义乌家医' }
    },
    {
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
    },
    {
        path: '**',//fallback router must in the last
        component: HomeComponent
    }
]
