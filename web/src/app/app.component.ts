import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { InterceptorService } from 'ng2-interceptors';

import { Title } from '@angular/platform-browser';

import { environment } from "../environments/environment";
import { WXService } from './services/wx.service';
import { SessionStorage } from './services/session.storage.service';

import { Logger } from "./error";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  private sub: any;

  constructor(public router: Router,
    private route: ActivatedRoute,
    private http: InterceptorService,
    public title: Title,
    private wxService: WXService,
    public ss: SessionStorage) {

    if (!this.ss.get(environment.openid)) {

      let code = new URLSearchParams(location.search.slice(1)).get('code');

      if (code) {

        this.http
          .get(`${environment.domain}/wechat/oauth?code=${code}`)
          .map(res => { return res.json(); })
          .catch(Logger.handleError)
          .subscribe((res) => {

            this.ss.set(environment.openid, res.detail);
          });
      } else {

        let redirect_uri = encodeURIComponent(window.location.href).toLowerCase();

        window.location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${environment.appid}&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect`;
      }
    }


    this.wxService.config({
      desc: '欢迎使用义务家医',
      imgUrl: '/asserts/images/wechat.png'
    }).then((data) => {
      // 其它操作，可以确保注册成功以后才有效
      console.log('注册成功')
    }).catch((err: string) => {
      console.log(`注册失败，原因：${err}`)
    });

    //this.ss.set(environment.openid, '111');

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.route)
      .map(route => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
      .filter(route => route.outlet === 'primary')
      .mergeMap(route => route.data)
      .subscribe((event) => this.title.setTitle(event['title']));
  }


  ngOnDestroy(): any {
    this.sub.unsubscribe();
  }

}
