import { TemplateRef } from '@angular/core';
import { Pager } from "./pager";
import { DoctorModel } from "./doctor.model";

export class ConsultationModel extends Pager {
    consultationId: string;
    openId: string;
    basicName: string;
    basicPhone: string;
    basicSex: string;
    basicAge: number;
    doctorId: string;
    doctor: DoctorModel;
    doctorName: string;
    consultationTitle: string;
    consultationDate: Date;
    consultationContent: string;
    consultationStatus: string;
    consultationStatusName: string;
    status: string;
    statusName: string;
    ctime: Date;
    mtime: Date;
    price: number;
    pictUrls: Array<String>;

    couponId: string;
    block: TemplateRef<any>;
}

