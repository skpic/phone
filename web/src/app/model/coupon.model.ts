import { Pager } from "./pager";

export class CouponModel extends Pager {
    couponId: string;
    name: string;
    description: string;
    type: string;
    price: number;
    priceDesc: string;
    expire: number;
    useDesc: string;
    ctime: Date;
    mtime: Date;
}































