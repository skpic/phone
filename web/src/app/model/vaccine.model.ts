import { Pager } from "./pager";
import { DoseModel } from "./dose.model";

export class VaccineModel extends Pager {
    vaccineId: string;
    name: string;
    description: string;
    position: string;
    contraindication: string;
    adverseReaction: string;
    status: string;
    total: number;
    number: number;
    type: string;
    vaccineTime: Date;
    ctime: Date;
    mtime: Date;
    dostList: Array<DoseModel>;
    wechatVaccineId: string;
}