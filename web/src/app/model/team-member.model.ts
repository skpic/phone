import { Pager } from "./pager";

export class TeamMemberModel extends Pager {
    teamMemberId: string;
    teamId: string;
    name: string;
    position: string;
    sortCode: string;
    pictUrl: number;
}