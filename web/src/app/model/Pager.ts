export class Pager {
    pageIndex: number;
    pageSize: number;
    total: number;
}
