import { Pager } from "./pager";

export class ConsultationPictModel extends Pager {
    consultationPictId: string;
    url: string;
    consultationId: string;
}

