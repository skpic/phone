import { Pager } from "./pager";
import { VaccineModel } from "./vaccine.model";

export class DoseModel extends Pager {
    doseId: string;
    openId: string;
    name: string;
    sort: number;
    type: string;
    vaccineTime: Date;
    vaccineList: Array<VaccineModel>;
    buttonCount: number;
    wechatVaccineId: string;
}