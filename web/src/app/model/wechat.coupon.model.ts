import { Pager } from "./pager";

export class WechatCouponModel extends Pager {
    wechatCouponId: string;
    couponId: string;
    name: string;
    description: string;
    type: string;
    price: number;
    priceDesc: string;
    expireDate: Date;
    expire: number;
    useDesc: string;
    ctime: Date;
    mtime: Date;
    openId: string;
    
    showDesc: boolean;
    
}





























