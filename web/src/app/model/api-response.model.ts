import { ResponseModel } from "./response.model";

export class ApiResponseModel<T> {
    response: ResponseModel;
    detail: T;
    list: Array<T>;
    total: number;
}
