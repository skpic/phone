export class SignedModel {
    name: string;
    status: string;
    startDate: string;
    endDate: string;
    count: string;
    doctor: string;
    department: string;
}