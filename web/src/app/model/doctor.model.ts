import { Pager } from "./pager";

export class DoctorModel extends Pager {
    doctorId: string;
    name: string;
    hospital: string;
    position: string;
    consultationCount: number;
    sortCode: number;
    summary: string;
    expertise: string;
    description: string;
    pictUrl: string;
    status: string;
    price: number;
}






























