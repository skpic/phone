import { Pager } from "./pager";

export class TeamModel extends Pager {
    teamId: string;
    name: string;
    position: string;
    address: string;
    type: string;
    signedCount: number;
    sortCode: number;
    icon: boolean;
    orgName: string;
    description: string;
}