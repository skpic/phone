
export class WechatVaccineModel {
    wechatVaccineId: string;
    openId: string;
    vaccineId: string;
    doseId: string;
    vaccineTime: Date;
}