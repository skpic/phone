import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { InterceptorService } from 'ng2-interceptors';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiResponseModel } from "../model/api-response.model";
import { environment } from "../../environments/environment";
import { DoctorModel } from "../model/doctor.model";
import { Logger } from "../error";

@Injectable()
export class DoctorService {

  constructor(private http: InterceptorService) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  public list(model: DoctorModel): Observable<ApiResponseModel<DoctorModel>> {

    var url: string = `${environment.domain}/doctor/list`;

    return this.http
      .post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => (res.json() as ApiResponseModel<DoctorModel>))
      .catch(Logger.handleError);
  }

  public add(model: DoctorModel): Observable<any> {

    var url: string = `${environment.domain}/doctor/add`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public edit(model: DoctorModel): Observable<any> {

    var url: string = `${environment.domain}/doctor/update`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public delete(id: string): Observable<any> {

    var url: string = `${environment.domain}/doctor/${id}/delete`;

    return this.http.post(url, JSON.stringify({}), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public get(id: string): Observable<ApiResponseModel<DoctorModel>> {

    var url: string = `${environment.domain}/doctor/${id}/get`;

    return this.http.get(url)
      .map((res: Response) => (res.json() as ApiResponseModel<DoctorModel>))
      .catch(Logger.handleError);
  }
}
