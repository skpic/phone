import { Injectable } from '@angular/core';
import {  Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { InterceptorService  } from 'ng2-interceptors';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//import { Md5 } from "ts-md5/dist/md5";

import { ApiResponseModel } from "../model/api-response.model";
import { environment } from "../../environments/environment";
import { ConsultationModel } from "../model/consultation.model";
import { Logger } from "../error";

@Injectable()
export class ConsultationService {

  constructor(private http: InterceptorService) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  public list(model: ConsultationModel): Observable<ApiResponseModel<ConsultationModel>> {

    var url: string = `${environment.domain}/consultation/list`;

    return this.http
      .post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => (res.json() as ApiResponseModel<ConsultationModel>))
      .catch(Logger.handleError);
  }

  public add(model: ConsultationModel): Observable<any> {

    var url: string = `${environment.domain}/consultation/add`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public edit(model: ConsultationModel): Observable<any> {

    var url: string = `${environment.domain}/consultation/update`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public finish(model: ConsultationModel): Observable<any> {

    var url: string = `${environment.domain}/consultation/finish`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public delete(id: string): Observable<any> {

    var url: string = `${environment.domain}/consultation/${id}/delete`;

    return this.http.post(url, JSON.stringify({}), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public get(id: string): Observable<ApiResponseModel<ConsultationModel>> {

    var url: string = `${environment.domain}/consultation/${id}/get`;

    return this.http.get(url)
      .map((res: Response) => (res.json() as ApiResponseModel<ConsultationModel>))
      .catch(Logger.handleError);
  }

  public pay(id: string): Observable<ApiResponseModel<string>> {

    var url: string = `${environment.domain}/consultation/pay/${id}/get`;

    return this.http.get(url)
      .map((res: Response) => (res.json() as ApiResponseModel<string>))
      .catch(Logger.handleError);
  }
}
