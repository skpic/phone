import { Observable } from 'rxjs/Rx';
import { InterceptorService } from 'ng2-interceptors';
import { Injectable } from '@angular/core';
import { JWeiXinService } from 'ngx-weui/jweixin';
import { environment } from "../../environments/environment";

declare const wx: any;
declare var WeixinJSBridge: any;

/**
 * 微信JS-SDK服务器
 */
@Injectable()
export class WXService {
    private static DEFAULTSHARE: any = {
        title: 'Site Name',
        desc: '',
        link: '',
        imgUrl: ''
    };
    constructor(private wxService: JWeiXinService, private http: InterceptorService) { }

    private share: any;
    config(shareData: any): Promise<boolean> {
        this.share = shareData;
        return new Promise((resolve, reject) => {
            this.wxService.get().then((res) => {
                if (!res) {
                    reject('jweixin.js 加载失败');
                    return;
                }

                wx.ready(() => {
                    this._onMenuShareTimeline()
                        ._onMenuShareAppMessage()
                        ._onMenuShareQQ()
                        ._onMenuShareQZone()
                        ._onMenuShareWeibo();

                    resolve();
                });
                wx.error(() => {
                    reject('config 注册失败');
                });

                this.http
                    .get(`${environment.domain}/wechat/sign?url=${window.location.href}`)
                    .map(res => { return res.json(); })
                    .catch((error: Response | any) => {
                        reject('无法获取签名数据');
                        return Observable.throw('error');
                    })
                    .subscribe((res) => {
                        if (!res.response || res.response.status != 200) {
                            reject('jsapi 获取失败');
                            return;
                        }
                        wx.config(res.detail);
                    });
            });
        });
    }

    private _onMenuShareTimeline() {
        wx.onMenuShareTimeline(Object.assign({}, WXService.DEFAULTSHARE, this.share));
        return this;
    }

    private _onMenuShareAppMessage() {
        wx.onMenuShareAppMessage(Object.assign({}, WXService.DEFAULTSHARE, this.share));
        return this;
    }

    private _onMenuShareQQ() {
        wx.onMenuShareQQ(Object.assign({}, WXService.DEFAULTSHARE, this.share));
        return this;
    }

    private _onMenuShareWeibo() {
        wx.onMenuShareWeibo(Object.assign({}, WXService.DEFAULTSHARE, this.share));
        return this;
    }

    private _onMenuShareQZone() {
        wx.onMenuShareQZone(Object.assign({}, WXService.DEFAULTSHARE, this.share));
        return this;
    }

    onBridgeReady(data): Observable<any> {

        /*return  new Observable(function (observer) {
            WeixinJSBridge.invoke('getBrandWCPayRequest', data, function (res) {
                if (res.err_msg == "get_brand_wcpay_request:ok") {
                    observer.next(200);
                    observer.complete();
                } else {
                    observer.next(500);
                    observer.complete();
                }    // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。 
            });
        })
 */
        return Observable.fromPromise(new Promise((resolve, reject) => {

            WeixinJSBridge.invoke('getBrandWCPayRequest', data, function (res) {
                if (res.err_msg == "get_brand_wcpay_request:ok") {
                    resolve();
                } else {
                    reject('支付失败');
                }    // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。 
            });
        }));


    }

}
