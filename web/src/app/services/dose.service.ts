import { Injectable } from '@angular/core';
import {Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { InterceptorService  } from 'ng2-interceptors';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//import { Md5 } from "ts-md5/dist/md5";

import { ApiResponseModel } from "../model/api-response.model";
import { environment } from "../../environments/environment";
import { DoseModel } from "../model/dose.model";
import { Logger } from "../error";

@Injectable()
export class DoseService {

  constructor(private http: InterceptorService) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  public list(model: DoseModel): Observable<ApiResponseModel<DoseModel>> {

    var url: string = `${environment.domain}/dose/list/vaccine`;

    return this.http
      .post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => (res.json() as ApiResponseModel<DoseModel>))
      .catch(Logger.handleError);
  }

  public add(model: DoseModel): Observable<any> {

    var url: string = `${environment.domain}/dose/add`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public edit(model: DoseModel): Observable<any> {

    var url: string = `${environment.domain}/dose/update`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public delete(id: string): Observable<any> {

    var url: string = `${environment.domain}/dose/${id}/delete`;

    return this.http.post(url, JSON.stringify({}), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public get(id: string): Observable<ApiResponseModel<DoseModel>> {

    var url: string = `${environment.domain}/dose/${id}/get`;

    return this.http.get(url)
      .map((res: Response) => (res.json() as ApiResponseModel<DoseModel>))
      .catch(Logger.handleError);
  }
}
