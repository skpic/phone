import { Injectable } from '@angular/core';
import {  Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { InterceptorService  } from 'ng2-interceptors';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//import { Md5 } from "ts-md5/dist/md5";

import { ApiResponseModel } from "../model/api-response.model";
import { environment } from "../../environments/environment";
import { TeamMemberModel } from "../model/team-member.model";
import { Logger } from "../error";

@Injectable()
export class TeamMemberService {

  constructor(private http: InterceptorService) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  public list(model: TeamMemberModel): Observable<ApiResponseModel<TeamMemberModel>> {

    var url: string = `${environment.domain}/team/member/list`;
    
    return this.http
      .post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => (res.json() as ApiResponseModel<TeamMemberModel>))
      .catch(Logger.handleError);
  }

  public add(model: TeamMemberModel): Observable<any> {

    var url: string = `${environment.domain}/team/member/add`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public edit(model: TeamMemberModel): Observable<any> {

    var url: string = `${environment.domain}/team/member/update`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public delete(id: string): Observable<any> {

    var url: string = `${environment.domain}/team/member/${id}/delete`;

    return this.http.post(url, JSON.stringify({}), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public get(id: string): Observable<ApiResponseModel<TeamMemberModel>> {

    var url: string = `${environment.domain}/team/member/${id}/get`;

    return this.http.get(url)
      .map((res: Response) => (res.json() as ApiResponseModel<TeamMemberModel>))
      .catch(Logger.handleError);
  }
}
