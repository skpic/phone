import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { InterceptorService } from 'ng2-interceptors';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//import { Md5 } from "ts-md5/dist/md5";

import { ApiResponseModel } from "../model/api-response.model";
import { environment } from "../../environments/environment";
import { TeamModel } from "../model/team.model";
import { Logger } from "../error";

@Injectable()
export class TeamService {

  constructor(private http: InterceptorService) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  public list(model: TeamModel): Observable<ApiResponseModel<TeamModel>> {

    var url: string = `${environment.domain}/team/list`;
    //var url: string = `data/team.json`;

    return this.http
      .post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => (res.json() as ApiResponseModel<TeamModel>))
      .catch(Logger.handleError);
  }

  public add(model: TeamModel): Observable<any> {

    var url: string = `${environment.domain}/team/add`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public edit(model: TeamModel): Observable<any> {

    var url: string = `${environment.domain}/team/update`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public delete(id: string): Observable<any> {

    var url: string = `${environment.domain}/team/${id}/delete`;

    return this.http.post(url, JSON.stringify({}), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public get(id: string): Observable<ApiResponseModel<TeamModel>> {

    var url: string = `${environment.domain}/team/${id}/get`;
    //var url: string = `data/team.json`;

    return this.http.get(url)
      .map((res: Response) => (res.json() as ApiResponseModel<TeamModel>))
      .catch(Logger.handleError);
  }
}
