import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { InterceptorService } from 'ng2-interceptors';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//import { Md5 } from "ts-md5/dist/md5";

import { ApiResponseModel } from "../model/api-response.model";
import { environment } from "../../environments/environment";
import { VaccineModel } from "../model/vaccine.model";
import { WechatVaccineModel } from "../model/wechat.vaccine.model";
import { Logger } from "../error";

@Injectable()
export class VaccineService {

  constructor(private http: InterceptorService) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  /**
   * 获取疫苗列表
   * @param model 疫苗列表查询参数
   */
  public list(model: VaccineModel): Observable<ApiResponseModel<VaccineModel>> {

    var url: string = `${environment.domain}/vaccine/list`;

    return this.http
      .post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => (res.json() as ApiResponseModel<VaccineModel>))
      .catch(Logger.handleError);
  }

  /**
   * 添加用户接种信息
   * @param model 用户接种信息
   */
  public inoculation(model: WechatVaccineModel): Observable<any> {

    var url: string = `${environment.domain}/wechat/vaccine/add`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  /**
   * 修改用户接种信息
   * @param model 用户接种信息
   */
  public changeInoculationDate(model: WechatVaccineModel): Observable<any> {

    var url: string = `${environment.domain}/wechat/vaccine/update`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  /**
   * 调用未接种接口
   * @param id 用户接种ID
   */
  public notInoculation(id: string): Observable<any> {

    var url: string = `${environment.domain}/wechat/vaccine/${id}/delete`;

    return this.http.post(url, JSON.stringify({}), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  /**
   * 获取用户接种疫苗信息（包括详情及所有接种剂次列表）
   * @param id 疫苗ID
   * @param openId 微信用户openid
   */
  public get(id: string, openId: string): Observable<ApiResponseModel<VaccineModel>> {

    var url: string = `${environment.domain}/vaccine/${id}/${openId}/get`;

    return this.http.get(url)
      .map((res: Response) => (res.json() as ApiResponseModel<VaccineModel>))
      .catch(Logger.handleError);
  }

}
