import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { InterceptorService } from 'ng2-interceptors';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiResponseModel } from "../model/api-response.model";
import { environment } from "../../environments/environment";
import { WechatCouponModel } from "../model/wechat.coupon.model";
import { Logger } from "../error";

@Injectable()
export class CouponService {

  constructor(private http: InterceptorService) { }

  private headers = new Headers({ 'Content-Type': 'application/json' });

  public list(model: WechatCouponModel): Observable<ApiResponseModel<WechatCouponModel>> {

    var url: string = `${environment.domain}/wechat/coupon/list`;

    return this.http
      .post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => (res.json() as ApiResponseModel<WechatCouponModel>))
      .catch(Logger.handleError);
  }

  public add(model: WechatCouponModel): Observable<any> {

    var url: string = `${environment.domain}/wechat/coupon/add`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public edit(model: WechatCouponModel): Observable<any> {

    var url: string = `${environment.domain}/wechat/coupon/update`;

    return this.http.post(url, JSON.stringify(model), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public delete(id: string): Observable<any> {

    var url: string = `${environment.domain}/wechat/coupon/${id}/delete`;

    return this.http.post(url, JSON.stringify({}), { headers: this.headers })
      .map((res: Response) => res.json())
      .catch(Logger.handleError);
  }

  public get(id: string): Observable<ApiResponseModel<WechatCouponModel>> {

    var url: string = `${environment.domain}/wechat/coupon/${id}/get`;

    return this.http.get(url)
      .map((res: Response) => (res.json() as ApiResponseModel<WechatCouponModel>))
      .catch(Logger.handleError);
  }
}
