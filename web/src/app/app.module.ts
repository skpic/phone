import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';

import { WeUiModule } from 'ngx-weui';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { SharedModule } from './shared/shared.module';

import { WXService } from './services/wx.service';
import { SessionStorage } from './services/session.storage.service';

import { appRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';

import { provideInterceptorService, InterceptorService } from 'ng2-interceptors';
import { ServerURLInterceptor } from './interceptor/server.url.interceptor';

import { ToastComponent, ToastService } from "ngx-weui/toast";

export function interceptorFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, interceptor: ServerURLInterceptor) {
  let service = new InterceptorService(xhrBackend, requestOptions);
  service.addInterceptor(interceptor);
  return service;
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    SharedModule,
    RouterModule.forRoot(appRoutes),
    WeUiModule.forRoot(),
    NgZorroAntdModule.forRoot()
  ],
  exports: [RouterModule],
  providers: [
    WXService,
    SessionStorage,
    ServerURLInterceptor,
    {
      provide: InterceptorService,
      useFactory: interceptorFactory,
      deps: [XHRBackend, RequestOptions, ServerURLInterceptor]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
